<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\Acudientes;

/**
 * AcudientesSearch represents the model behind the search form of `app\models\Acudientes`.
 */
class AcudientesSearch extends Acudientes
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_acudiente', 'telefono_fijo', 'completo'], 'integer'],
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'direccion', 'ciudad', 'email'], 'safe'],
            [['celular'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = Acudientes::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_acudiente' => $this->id_acudiente,
            'telefono_fijo' => $this->telefono_fijo,
            'celular' => $this->celular,
            'completo' => $this->completo,
        ]);

        $query->andFilterWhere(['ilike', 'tipo_documento', $this->tipo_documento])
            ->andFilterWhere(['ilike', 'numero_documento', $this->numero_documento])
            ->andFilterWhere(['ilike', 'primer_nombre', $this->primer_nombre])
            ->andFilterWhere(['ilike', 'segundo_nombre', $this->segundo_nombre])
            ->andFilterWhere(['ilike', 'primer_apellido', $this->primer_apellido])
            ->andFilterWhere(['ilike', 'segundo_apellido', $this->segundo_apellido])
            ->andFilterWhere(['ilike', 'direccion', $this->direccion])
            ->andFilterWhere(['ilike', 'ciudad', $this->ciudad])
            ->andFilterWhere(['ilike', 'email', $this->email]);

        return $dataProvider;
    }
}
