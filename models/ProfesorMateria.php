<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesor_materia".
 *
 * @property int $id_profesor_materia
 * @property int $id_profesor
 * @property int $id_materia
 *
 * @property Asignaturas $materia
 * @property Profesor $profesor
 */
class ProfesorMateria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesor_materia';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_profesor', 'id_materia'], 'required','message'=>'Campo Rquerido'],
            [['id_profesor', 'id_materia'], 'default', 'value' => null],
            [['id_profesor', 'id_materia'], 'integer'],
            [['id_profesor', 'id_materia'], 'unique', 'targetAttribute' => ['id_profesor', 'id_materia']],
            [['id_materia'], 'exist', 'skipOnError' => true, 'targetClass' => Asignaturas::className(), 'targetAttribute' => ['id_materia' => 'id_asignatura']],
            [['id_profesor'], 'exist', 'skipOnError' => true, 'targetClass' => Profesor::className(), 'targetAttribute' => ['id_profesor' => 'id_profesor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_profesor_materia' => 'Id Profesor Materia',
            'id_profesor' => 'Profesor',
            'id_materia' => 'Asignatura',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMateria()
    {
        return $this->hasOne(Asignaturas::className(), ['id_asignatura' => 'id_materia']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesor()
    {
        return $this->hasOne(Profesor::className(), ['id_profesor' => 'id_profesor']);
    }
}
