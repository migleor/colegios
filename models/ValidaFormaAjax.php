<?php

namespace app\models;
use Yii;
use yii\base\Model;

class ValidaFormaAjax extends Model{
    public $nombre;
    public $email;
    
    public function rules(){
        return [
            ["nombre","required","message"=>"Campo Requerido"],
            ["nombre","match","pattern"=>"/^.{3,50}$/","message"=>"Minimo 3 maximo 50 caracteres"],
            ["nombre","match","pattern"=>"/^[0-9a-z]+$/i","message"=>"Solo se aceptan letras y numeros"],
            ["email","required","message"=>"Campo Requerido"],
            ["email","match","pattern"=>"/^.{5,80}$/","message"=>"Minimo 3 maximo 50 caracteres"],
            ["email","email","message"=>"Formato de email no válido"],
            ["email","email_existe"]
        ];
    }
    public function attributeLabels(){
        return [
            "nombre"=>"Ingrese su Nombre:",
            "email"=>"Ingrese su Email:"
        ];
    }
    
    public function email_existe($attribute,$params){
        $email = ["migleor@gmail.com","miguel.ortega@imagineltda.com"];
        foreach($email as $key){
            if($this->email == $key){
                $this->addError("Email seleccionado existe");
                return true;
            }else{
                return false;
            }
        }
    }
}

