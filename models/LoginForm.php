<?php

namespace app\models;

use Yii;
use yii\base\Model;
use app\models\Usuarios;
use app\models\Perfiles;
use app\models\Alumnos;
use app\models\Profesor;
use app\models\Acudientes;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class LoginForm extends Model
{
    public $username;
    public $password;
    public $perfil;
    public $userid;
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username', 'password','perfil'], 'required','message'=>'Campo Obligatorio'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

    /**
     * Validates the password.
     * This method serves as the inline validation for password.
     *
     * @param string $attribute the attribute currently being validated
     * @param array $params the additional name-value pairs given in the rule
     */
    public function validatePassword($attribute, $params)
    {
        //$this->addError($attribute, $this->username);
        $passEncode = null;
        //con el perfil y el usuario buscamos el id de usuario para luego cruzar con el password
        $dataPerfil = Perfiles::findOne(["id_perfil"=>$this->perfil]);
        $row = Usuarios::find()
                ->select(['id_usuario','password'])
                ->innerJoin($dataPerfil->tabla, 'usuarios.id_actor = '.$dataPerfil->nombre_id)
                ->where(['numero_documento'=>$this->username])
                ->one();
        if(!$row){
            $this->addError($attribute, 'Usuario o password no válidos');
        }else{
            //validamos si el usuario y password ingresado existen
            $passEncode = base64_encode($this->password);
            $row2 = Usuarios::find()
                    ->where(['id_usuario'=>$row->id_usuario,'password'=>$passEncode])
                    ->one();
            if(!$row2){
                $this->addError($attribute, 'Usuario o password no válidos');
            }else{
                $this->userid = $row2->id_usuario;
            }
        }        
    }

}
