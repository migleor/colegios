<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "grados".
 *
 * @property int $id_grado
 * @property string $grado
 */
class Grados extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'grados';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_grado'], 'required'],
            [['id_grado'], 'default', 'value' => null],
            [['id_grado'], 'integer'],
            [['grado'], 'string'],
            [['id_grado'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_grado' => 'Id Grado',
            'grado' => 'Grado',
        ];
    }
}
