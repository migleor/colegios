<?php

namespace app\models;
use app\models\Usuarios;

use Yii;

/**
 * This is the model class for table "mensajeria".
 *
 * @property int $id_mensaje
 * @property string $asunto
 * @property int $id_remitente
 * @property int $id_destinatario
 * @property int $perfil_remitente
 * @property string $mensaje
 * @property string $fecha_envio_mensaje
 */
class Mensajeria extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'mensajeria';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['asunto', 'mensaje'], 'string'],
            [['id_remitente', 'id_destinatario', 'perfil_remitente'], 'default', 'value' => null],
            [['id_destinatario','asunto','mensaje'], 'required', 'message' => "Campo Requerido"],
            [['id_remitente', 'id_destinatario', 'perfil_remitente'], 'integer'],
            [['fecha_envio_mensaje'], 'safe'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_mensaje' => 'Id Mensaje',
            'asunto' => 'Asunto',
            'id_remitente' => 'Id Remitente',
            'id_destinatario' => 'Id Destinatario',
            'perfil_remitente' => 'Perfil Remitente',
            'mensaje' => 'Mensaje',
            'fecha_envio_mensaje' => 'Fecha Envio Mensaje',
        ];
    }
    public function getUsuarioActor($id_actor){
        $usu = Usuarios::find()
            ->where(["id_actor"=>$id_actor])
            ->one();
        if($usu){
            return $usu->id_usuario;
        }else{
            return "";
        }
    }
    public function getInfoUsuario($id_usuario,$campo){
        //traemos el perfil y el id del actor
        $usu = Usuarios::findOne($id_usuario);
        if($usu){
            switch($usu->id_perfil){
                case 1:
                   //es un progfesor, traemos la data de ese profesor 
                   $item = Profesor::findOne($usu->id_actor);
                break;
                case 2:
                   //alumnos
                   $item = Alumnos::findOne($usu->id_actor);                    
                break;
                case 3:
                    //acudientes
                    $item = Acudientes::findOne($usu->id_actor);
                break;
            }
            return $item->$campo;
        }else{
            return "";
        }
    }
    public function sendMail($content,$email,$asunto){
        Yii::$app->mailer->compose("@app/mail/layouts/html", ["content" => $content])
        ->setTo($email)
        ->setFrom(["miguel.ortega@imagineltda.com" => "We-Colegios"])
        ->setSubject($asunto)
        ->setTextBody($content)
        ->send();
    }
}
