<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "parentesco".
 *
 * @property int $id_parentesco
 * @property string $parentesco
 * @property int $estado_parentesco
 */
class Parentesco extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'parentesco';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['parentesco'], 'string'],
            [['estado_parentesco'], 'default', 'value' => null],
            [['estado_parentesco'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_parentesco' => 'Id Parentesco',
            'parentesco' => 'Parentesco',
            'estado_parentesco' => 'Estado Parentesco',
        ];
    }
}
