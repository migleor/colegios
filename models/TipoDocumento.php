<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tipo_documento".
 *
 * @property string $id_tipo
 * @property string $tipo
 * @property int $estado
 */
class TipoDocumento extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tipo_documento';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_tipo'], 'required'],
            [['id_tipo', 'tipo'], 'string'],
            [['estado'], 'default', 'value' => null],
            [['estado'], 'integer'],
            [['id_tipo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_tipo' => 'Id Tipo',
            'tipo' => 'Tipo',
            'estado' => 'Estado',
        ];
    }
}
