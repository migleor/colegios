<?php

namespace app\models;
use Yii;
use yii\base\Model;

class SetearPassword extends Model{
    public $password;
    public $passrepeat;
    public $userid;
    public function rules(){
        return[
          ['userid','required','message'=>'El id del usuario no puede ser nulo'],
          ['password','required','message'=>'Ingrese el Password'],
          ['passrepeat','required','message'=>'Confirme el Password'],
          ['passrepeat', 'compare', 'compareAttribute' => 'password', 'message' => 'Los passwords no coinciden'],
        ];
    }
    public function attributeLabels(){
        return [
            "password"=>"Password",
            "passrepeat"=>"Confirmar Password"
        ];
    }
}
