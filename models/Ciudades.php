<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ciudades".
 *
 * @property string $codigo
 * @property string $codigo_depto
 * @property string $nombre_depto
 * @property string $codigo_ciudad
 * @property string $nombre_ciudad
 */
class Ciudades extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ciudades';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['codigo'], 'required'],
            [['codigo', 'codigo_depto', 'nombre_depto', 'codigo_ciudad', 'nombre_ciudad'], 'string'],
            [['codigo'], 'unique'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'codigo' => 'Codigo',
            'codigo_depto' => 'Codigo Depto',
            'nombre_depto' => 'Nombre Depto',
            'codigo_ciudad' => 'Codigo Ciudad',
            'nombre_ciudad' => 'Nombre Ciudad',
        ];
    }
}
