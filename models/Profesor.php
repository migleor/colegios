<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesor".
 *
 * @property int $id_profesor
 * @property string $tipo_documento
 * @property string $numero_documento
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property int $telefono
 * @property string $celular
 * @property string $direccion
 * @property string $id_ciudad
 * @property string $codigo
 * @property string $email
 * @property int $completo
 *
 * @property Mensajeria[] $mensajerias
 * @property Ciudades $ciudad
 * @property TipoDocumento $tipoDocumento
 * @property ProfesorAlumno[] $profesorAlumnos
 * @property Alumnos[] $alumnos
 * @property ProfesorMateria[] $profesorMaterias
 * @property Asignaturas[] $materias
 */
class Profesor extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesor';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'direccion', 'id_ciudad', 'codigo', 'email'], 'string'],
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'primer_apellido','celular', 'email'], 'required','message'=>'Campo Requerido'],
            ['email','email','message'=>'Formato de email inválido'],
            [['telefono', 'completo'], 'default', 'value' => null],
            [['telefono', 'completo'], 'integer'],
            [['celular'], 'number'],
            [['numero_documento'], 'unique','message'=>'El numero de identificación ya existe en la base de datos'],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudades::className(), 'targetAttribute' => ['id_ciudad' => 'codigo']],
            [['tipo_documento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento' => 'id_tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_profesor' => 'Id Profesor',
            'tipo_documento' => 'Tipo Documento',
            'numero_documento' => 'Numero Documento',
            'primer_nombre' => 'Primer Nombre',
            'segundo_nombre' => 'Segundo Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'direccion' => 'Direccion',
            'id_ciudad' => 'Id Ciudad',
            'codigo' => 'Codigo',
            'email' => 'Email',
            'completo' => 'Completo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMensajerias()
    {
        return $this->hasMany(Mensajeria::className(), ['id_profesor' => 'id_profesor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudades::className(), ['codigo' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id_tipo' => 'tipo_documento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesorAlumnos()
    {
        return $this->hasMany(ProfesorAlumno::className(), ['id_profesor' => 'id_profesor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumnos()
    {
        return $this->hasMany(Alumnos::className(), ['id_alumno' => 'id_alumno'])->viaTable('profesor_alumno', ['id_profesor' => 'id_profesor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesorMaterias()
    {
        return $this->hasMany(ProfesorMateria::className(), ['id_profesor' => 'id_profesor']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMaterias()
    {
        return $this->hasMany(Asignaturas::className(), ['id_asignatura' => 'id_materia'])->viaTable('profesor_materia', ['id_profesor' => 'id_profesor']);
    }
}
