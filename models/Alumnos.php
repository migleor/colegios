<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "alumnos".
 *
 * @property int $id_alumno
 * @property string $tipo_documento
 * @property string $numero_documento
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property int $grado
 * @property int $telefono
 * @property string $celular
 * @property string $direccion
 * @property string $id_ciudad
 * @property string $codigo
 * @property string $email
 * @property int $completo
 *
 * @property Ciudades $ciudad
 * @property Grados $grado0
 * @property TipoDocumento $tipoDocumento
 * @property EstudianteAcudiente[] $estudianteAcudientes
 * @property Acudientes[] $acudientes
 * @property ProfesorAlumno[] $profesorAlumnos
 * @property Profesor[] $profesors
 */
class Alumnos extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'alumnos';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'direccion', 'id_ciudad', 'codigo', 'email'], 'string'],
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'primer_apellido', 'email','celular'], 'required',"message"=>"Campo Requerido"],
            ['email','email','message'=>"Formato de email invalido"],
            [['grado', 'telefono', 'completo'], 'default', 'value' => null],
            [['grado', 'telefono', 'completo'], 'integer'],
            [['celular'], 'number'],
            [['numero_documento'], 'unique','message'=>'El alumno ya se encuentra registrado en el sistema'],
            [['id_ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudades::className(), 'targetAttribute' => ['id_ciudad' => 'codigo']],
            [['grado'], 'exist', 'skipOnError' => true, 'targetClass' => Grados::className(), 'targetAttribute' => ['grado' => 'id_grado']],
            [['tipo_documento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento' => 'id_tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_alumno' => 'Id Alumno',
            'tipo_documento' => 'Tipo Documento',
            'numero_documento' => 'Numero Documento',
            'primer_nombre' => 'Primer Nombre',
            'segundo_nombre' => 'Segundo Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'grado' => 'Grado',
            'telefono' => 'Telefono',
            'celular' => 'Celular',
            'direccion' => 'Direccion',
            'id_ciudad' => 'Id Ciudad',
            'codigo' => 'Codigo',
            'email' => 'Email',
            'completo' => 'Completo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad()
    {
        return $this->hasOne(Ciudades::className(), ['codigo' => 'id_ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getGrado0()
    {
        return $this->hasOne(Grados::className(), ['id_grado' => 'grado']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id_tipo' => 'tipo_documento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudianteAcudientes()
    {
        return $this->hasMany(EstudianteAcudiente::className(), ['id_estudiante' => 'id_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcudientes()
    {
        return $this->hasMany(Acudientes::className(), ['id_acudiente' => 'id_acudiente'])->viaTable('estudiante_acudiente', ['id_estudiante' => 'id_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesorAlumnos()
    {
        return $this->hasMany(ProfesorAlumno::className(), ['id_alumno' => 'id_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesors()
    {
        return $this->hasMany(Profesor::className(), ['id_profesor' => 'id_profesor'])->viaTable('profesor_alumno', ['id_alumno' => 'id_alumno']);
    }
}
