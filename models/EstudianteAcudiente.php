<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "estudiante_acudiente".
 *
 * @property int $id_estudiante_acudiente
 * @property int $id_estudiante
 * @property int $id_acudiente
 *
 * @property Acudientes $acudiente
 * @property Alumnos $estudiante
 */
class EstudianteAcudiente extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'estudiante_acudiente';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_estudiante', 'id_acudiente'], 'default', 'value' => null],
            [['id_estudiante', 'id_acudiente'], 'integer'],
            [['id_estudiante', 'id_acudiente'], 'required','message'=>'Campo Requerido'],
            [['id_estudiante', 'id_acudiente'], 'unique', 'targetAttribute' => ['id_estudiante', 'id_acudiente'],'message'=>'Ya existe la relacion Alumno Acudiente'],
            [['id_acudiente'], 'exist', 'skipOnError' => true, 'targetClass' => Acudientes::className(), 'targetAttribute' => ['id_acudiente' => 'id_acudiente']],
            [['id_estudiante'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['id_estudiante' => 'id_alumno']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_estudiante_acudiente' => 'Id Estudiante Acudiente',
            'id_estudiante' => 'Id Estudiante',
            'id_acudiente' => 'Id Acudiente',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAcudiente()
    {
        return $this->hasOne(Acudientes::className(), ['id_acudiente' => 'id_acudiente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiante()
    {
        return $this->hasOne(Alumnos::className(), ['id_alumno' => 'id_estudiante']);
    }
}
