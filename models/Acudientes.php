<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "acudientes".
 *
 * @property int $id_acudiente
 * @property string $tipo_documento
 * @property string $numero_documento
 * @property string $primer_nombre
 * @property string $segundo_nombre
 * @property string $primer_apellido
 * @property string $segundo_apellido
 * @property int $telefono_fijo
 * @property string $celular
 * @property string $direccion
 * @property string $ciudad
 * @property string $email
 * @property int $completo
 *
 * @property Ciudades $ciudad0
 * @property TipoDocumento $tipoDocumento
 * @property EstudianteAcudiente[] $estudianteAcudientes
 * @property Alumnos[] $estudiantes
 */
class Acudientes extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'acudientes';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'segundo_nombre', 'primer_apellido', 'segundo_apellido', 'direccion', 'ciudad', 'email'], 'string'],
            [['tipo_documento', 'numero_documento', 'primer_nombre', 'primer_apellido', 'celular', 'email'], 'required','message'=>'Campo Requerido'],
            ['email','email','message'=>'Formato De Email Inválido'],
            [['telefono_fijo', 'completo'], 'default', 'value' => null],
            [['telefono_fijo', 'completo'], 'integer'],
            [['celular'], 'number'],
            [['numero_documento'], 'unique','message'=>'El numero de identificación ya se encuentra registrado en el sistema'],
            [['ciudad'], 'exist', 'skipOnError' => true, 'targetClass' => Ciudades::className(), 'targetAttribute' => ['ciudad' => 'codigo']],
            [['tipo_documento'], 'exist', 'skipOnError' => true, 'targetClass' => TipoDocumento::className(), 'targetAttribute' => ['tipo_documento' => 'id_tipo']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_acudiente' => 'Id Acudiente',
            'tipo_documento' => 'Tipo Documento',
            'numero_documento' => 'Numero Documento',
            'primer_nombre' => 'Primer Nombre',
            'segundo_nombre' => 'Segundo Nombre',
            'primer_apellido' => 'Primer Apellido',
            'segundo_apellido' => 'Segundo Apellido',
            'telefono_fijo' => 'Telefono',
            'celular' => 'Celular',
            'direccion' => 'Direccion',
            'ciudad' => 'Ciudad',
            'email' => 'Email',
            'completo' => 'Completo',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCiudad0()
    {
        return $this->hasOne(Ciudades::className(), ['codigo' => 'ciudad']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTipoDocumento()
    {
        return $this->hasOne(TipoDocumento::className(), ['id_tipo' => 'tipo_documento']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudianteAcudientes()
    {
        return $this->hasMany(EstudianteAcudiente::className(), ['id_acudiente' => 'id_acudiente']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getEstudiantes()
    {
        return $this->hasMany(Alumnos::className(), ['id_alumno' => 'id_estudiante'])->viaTable('estudiante_acudiente', ['id_acudiente' => 'id_acudiente']);
    }
}
