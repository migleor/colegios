<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use app\models\EstudianteAcudiente;

/**
 * EstudianteAcudienteSearch represents the model behind the search form of `app\models\EstudianteAcudiente`.
 */
class EstudianteAcudienteSearch extends EstudianteAcudiente
{
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_estudiante_acudiente', 'id_estudiante', 'id_acudiente'], 'integer'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = EstudianteAcudiente::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'id_estudiante_acudiente' => $this->id_estudiante_acudiente,
            'id_estudiante' => $this->id_estudiante,
            'id_acudiente' => $this->id_acudiente,
        ]);

        return $dataProvider;
    }
}
