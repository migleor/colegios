<?php

namespace app\models;
use Yii;
use yii\base\Model;
use app\models\Alumnos;

class FormAlumnos extends Model{

public $id_alumno;
public $tipo_documento;
public $numero_documento;
public $primer_nombre;
public $segundo_nombre;
public $primer_apellido;
public $segundo_apellido;
public $grado;
public $telefono;
public $celular;
public $direccion;
public $id_ciudad;
public $codigo;

public function rules(){
  return [
    ['id_alumno', 'integer', 'message' => 'Id incorrecto'],
    ['tipo_documento', 'required', 'message' => 'Campo Requerido'],
    ['numero_documento', 'required', 'message' => 'Campo Requerido'],
    ['numero_documento','existeIdAlumno'],
    ['primer_nombre', 'required', 'message' => 'Campo requerido'],
    ['primer_nombre', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
    ['primer_nombre', 'match', 'pattern' => '/^.{3,50}$/', 'message' => 'Mínimo 3 máximo 50 caracteres'],
    ['segundo_nombre', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
    ['segundo_nombre', 'match', 'pattern' => '/^.{3,50}$/', 'message' => 'Mínimo 3 máximo 50 caracteres'],      
    ['primer_apellido', 'required', 'message' => 'Campo requerido'],
    ['primer_apellido', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
    ['primer_apellido', 'match', 'pattern' => '/^.{3,80}$/', 'message' => 'Mínimo 3 máximo 80 caracteres'],
    ['segundo_apellido', 'match', 'pattern' => '/^[a-záéíóúñ\s]+$/i', 'message' => 'Sólo se aceptan letras'],
    ['segundo_apellido', 'match', 'pattern' => '/^.{3,80}$/', 'message' => 'Mínimo 3 máximo 80 caracteres'],
    ['grado', 'required', 'message' => 'Campo requerido'],
    ['grado', 'integer', 'message' => 'Sólo números enteros'],
    ['telefono', 'required', 'message' => 'Campo requerido'],
    ['celular', 'required', 'message' => 'Campo requerido'],
    ['direccion', 'required', 'message' => 'Campo requerido'],
    ['id_ciudad', 'required', 'message' => 'Campo requerido'],
    ['codigo','required','message'=>'Campo Requerido'],
  ];
 }
 public function attributeLabels() {
    return [
        "id_alumno"=>"Id Alumno:",
        "tipo_documento"=>"Tipo Documento:",
        "numero_documento"=>"Numero Identificación:",
        "primer_nombre"=>"Primer Nombre:",
        "segundo_nombre"=>"Segundo Nombre:",
        "primer_apellido"=>"Primer Apellido:",
        "segundo_apellido"=>"Segundo Apellido:",
        "grado"=>"Grado:",
        "telefono"=>"Telefono Fijo:",
        "celular"=>"Telefono Celular:",
        "direccion"=>"Dirección Residencia:",
        "id_ciudad"=>"Ciudad Residencia:",
        "codigo"=>"Codigo Interno:",
    ];
 }
 public function existeIdAlumno($attribute,$params){
    $existe = Alumnos::find()->where(['numero_documento'=>$this->numero_documento])->exists();
    if($existe){
        $this->addError("El numero de documento ya existe");
        return true;
    }else{
        return false;
    }
 }
}
