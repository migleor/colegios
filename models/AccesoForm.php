<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\Query;
use app\models\Usuarios;
use app\models\Perfiles;
use app\models\Alumnos;
use app\models\Profesor;
use app\models\Acudientes;

/**
 * LoginForm is the model behind the login form.
 *
 * @property User|null $user This property is read-only.
 *
 */
class AccesoForm extends Model
{
    public $username;
    public $perfil;
    
    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            [['username','perfil'], 'required','message'=>'Campo Requerido'],
        ];
    }
    public function firstTime(){
        //tenemos el usuario y el perfil de acuerdo con el perfil buscamos la tabla que corresponda
        $idRow = null;
        $pass  = '';
        $dataPerfil = Perfiles::findOne(["id_perfil"=>$this->perfil]);
        $row = Usuarios::find()
                ->select(['id_usuario','password'])
                ->innerJoin($dataPerfil->tabla, 'usuarios.id_actor = '.$dataPerfil->nombre_id)
                ->where(['numero_documento'=>$this->username])
                ->one();
        if(!$row){
            $res["status"]="No User";
        }elseif($row->password==null){
            $res["status"]="setear";
            $res["userid"]=$row->id_usuario;
        }else{
            $res["status"]="login";
            $res["perfil"]=$this->perfil;
            $res["username"]=$this->username;
        }
        return $res;
    }
    public function getUserId(){
        return true;
    }
}
