<?php

namespace app\models;

use Yii;
use yii\base\Model;

class AlumnoSearch extends Model {
    public $q;
    public function rules(){
        return [
            ["q","match","pattern"=>"/^[0-9a-záéíóúñ\s]+$/i","message"=>"Ingrese solo letras o numeros"]
        ];
    }
    public function attributeLabels() {
        return [
            'q'=>"Buscar"
        ];
    }
}
?>

