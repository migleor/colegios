<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "profesor_alumno".
 *
 * @property int $id_profesor_alumno
 * @property int $id_profesor
 * @property int $id_alumno
 *
 * @property Alumnos $alumno
 * @property Profesor $profesor
 */
class ProfesorAlumno extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'profesor_alumno';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['id_profesor', 'id_alumno'], 'default', 'value' => null],
            [['id_profesor', 'id_alumno'], 'integer'],
            [['id_profesor', 'id_alumno'], 'required','message'=>'Campo Requerido'],
            [['id_profesor', 'id_alumno'], 'unique', 'targetAttribute' => ['id_profesor', 'id_alumno'],'message'=>'Ya existe la Relación profesor alumno'],
            [['id_alumno'], 'exist', 'skipOnError' => true, 'targetClass' => Alumnos::className(), 'targetAttribute' => ['id_alumno' => 'id_alumno']],
            [['id_profesor'], 'exist', 'skipOnError' => true, 'targetClass' => Profesor::className(), 'targetAttribute' => ['id_profesor' => 'id_profesor']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id_profesor_alumno' => 'Id Profesor Alumno',
            'id_profesor' => 'Id Profesor',
            'id_alumno' => 'Id Alumno',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAlumno()
    {
        return $this->hasOne(Alumnos::className(), ['id_alumno' => 'id_alumno']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getProfesor()
    {
        return $this->hasOne(Profesor::className(), ['id_profesor' => 'id_profesor']);
    }
}
