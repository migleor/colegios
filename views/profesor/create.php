<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Profesor */
$this->title = 'Registrar Profesor';
$this->params['breadcrumbs'][] = ['label' => 'Profesors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesor-create">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
