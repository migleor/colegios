<?php

use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Ciudades;

/* @var $this yii\web\View */
/* @var $model app\models\Profesor */

$this->title = strtoupper($model->primer_nombre.' '.$model->segundo_nombre.' '.$model->primer_apellido.' '.$model->segundo_apellido);
$this->params['breadcrumbs'][] = ['label' => 'Profesors', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesor-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_profesor], ['class' => 'btn btn-primary']) ?>
        <!--<?= Html::a('Delete', ['delete', 'id' => $model->id_profesor], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_profesor',
            'tipo_documento',
            'numero_documento',
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'telefono',
            'celular',
            //'direccion',
            [
                'label' => 'Ciudad',
                'value'=>function ($model) {
                    $ciu = Ciudades::findOne($model->id_ciudad);
                    if($ciu){
                        return strtoupper($ciu->nombre_ciudad.'- '.$ciu->nombre_depto);
                    }else{
                        return $model->id_ciudad;
                    }
                    
                },
            ], 
            //'codigo',
            'email:email',
            //'completo',
        ],
    ]) ?>

</div>
