<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesorSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profesors';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesor-index">
    <p>
        <?= Html::a('Registrar Profesor', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],        
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_profesor',
            'tipo_documento',
            'numero_documento',
            [
                'header' => 'Nombre',
                'value'=>function ($model) {
                    return strtoupper($model->primer_nombre.' '.$model->segundo_nombre.' '.$model->primer_apellido.' '.$model->segundo_apellido);
                },
            ],
            //'telefono',
            'celular',
            //'direccion',
            //'id_ciudad',
            //'codigo',
            'email:email',
            //'completo',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>
</div>
