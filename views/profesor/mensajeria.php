<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Alumnos;
$this->title = 'Envio de Mensajes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 order-md-1">
    <?= $msg ?>
    <?php $form = ActiveForm::begin([
        "method" => "post",
        'enableClientValidation' => true,
    ]);
    
    switch($perfil){
        case 1:
            //Profesor a alumno
            $combo = $form->field($model, 'id_destinatario')->dropDownList(ArrayHelper::map($table::find()->all(), 'id_alumno', 
                    function($table){
                            return strtoupper($table['primer_nombre'].' '.$table['segundo_nombre'].' '.$table['primer_apellido'].' '.$table['segundo_apellido']);                        
                        }
                    ),["prompt"=>"Seleccionar..."])->label('<b>Alumno:</b>');
        break;
        case 2:
            //Alumno a profesor
            $combo = $form->field($model, 'id_destinatario')->dropDownList(ArrayHelper::map($table::find()->all(), 'id_profesor', 
                    function($table){
                            return strtoupper($table['primer_nombre'].' '.$table['segundo_nombre'].' '.$table['primer_apellido'].' '.$table['segundo_apellido']);                        
                        }
                    ),["prompt"=>"Seleccionar..."])->label('<b>Profesor:</b>');        
        break;
        case 3:
            //Acudiente a profesor
            $combo = $form->field($model, 'id_destinatario')->dropDownList(ArrayHelper::map($table::find()->all(), 'id_profesor', 
                    function($table){
                            return strtoupper($table['primer_nombre'].' '.$table['segundo_nombre'].' '.$table['primer_apellido'].' '.$table['segundo_apellido']);                        
                        }
                    ),["prompt"=>"Seleccionar..."])->label('<b>Profesor:</b>');        
        break;
    }    
    ?>
    <div class="row">
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'id_remitente')->hiddenInput(["value"=>$usuario])->label(false); ?>
            <?= $form->field($model, 'perfil_remitente')->hiddenInput(["value"=>$perfil])->label(false); ?>
            <?= $combo ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'asunto')->textInput()->label('<b>Asunto:</b>') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-8 mb-3">
            <?= $form->field($model, 'mensaje')->widget(\yii\redactor\widgets\Redactor::className())->label('<b>Mensaje:</b>') ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-4 mb-3">
            <?= Html::submitButton("Enviar Mensaje", ["class" => "btn btn-primary"]) ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>    

