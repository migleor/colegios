<?php
use yii\helpers\Html;
/* @var $this yii\web\View */
/* @var $model app\models\Acudientes */
$this->title = 'Registrar Acudientes';
$this->params['breadcrumbs'][] = ['label' => 'Acudientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acudientes-create">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
