<?php
use yii\helpers\Html;
use yii\widgets\DetailView;
use app\models\Ciudades;
/* @var $this yii\web\View */
/* @var $model app\models\Acudientes */
$this->title = strtoupper($model->primer_nombre.' '.$model->segundo_nombre.' '.$model->primer_apellido.' '.$model->segundo_apellido);
$this->params['breadcrumbs'][] = ['label' => 'Acudientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acudientes-view">
    <p>
        <?= Html::a('Update', ['update', 'id' => $model->id_acudiente], ['class' => 'btn btn-primary']) ?>
        <!--<?= Html::a('Delete', ['delete', 'id' => $model->id_acudiente], [
            'class' => 'btn btn-danger',
            'data' => [
                'confirm' => 'Are you sure you want to delete this item?',
                'method' => 'post',
            ],
        ]) ?>-->
    </p>
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            //'id_acudiente',
            'tipo_documento',
            'numero_documento',
            'primer_nombre',
            'segundo_nombre',
            'primer_apellido',
            'segundo_apellido',
            'telefono_fijo',
            'celular',
            'direccion',
            [
                'label' => 'Ciudad',
                'value'=>function ($model) {
                    $ciu = Ciudades::findOne($model->ciudad);
                    if($ciu){
                        return strtoupper($ciu->nombre_ciudad.'-'.$ciu->nombre_depto);
                    }else{
                        $model->ciudad;
                    }   
                },
            ], 
            'email:email',
            //'completo',
        ],
    ]) ?>
</div>
