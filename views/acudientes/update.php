<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Acudientes */

$this->title = 'Update Acudientes: ' . $model->id_acudiente;
$this->params['breadcrumbs'][] = ['label' => 'Acudientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_acudiente, 'url' => ['view', 'id' => $model->id_acudiente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="acudientes-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
