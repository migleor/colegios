<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\AcudientesSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Acudientes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="acudientes-index">
    <p>
        <?= Html::a('Registrar Acudientes', ['create'], ['class' => 'btn btn-success']) ?>
    </p>
    <?= GridView::widget([
        'tableOptions' => [
            'class' => 'table table-striped',
        ],
        'options' => [
            'class' => 'table-responsive',
        ],        
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],
            //'id_acudiente',
            'tipo_documento',
            'numero_documento',
            [
                'header' => 'Nombre',
                'value'=>function ($model) {
                    return strtoupper($model->primer_nombre.' '.$model->segundo_nombre.' '.$model->primer_apellido.' '.$model->segundo_apellido);
                },
            ],
            //'telefono_fijo',
            'celular',
            //'direccion',
            //'ciudad',
            'email:email',
            //'completo',
            [
                'class' => 'yii\grid\ActionColumn',
                'template' => '{view} {update}',
            ],
        ],
    ]); ?>
</div>
