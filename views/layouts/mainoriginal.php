<?php

/* @var $this \yii\web\View */
/* @var $content string */

use app\widgets\Alert;
use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use yii\helpers\Url;
use app\assets\AppAsset;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => 'WE - COLEGIOS',
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar-inverse navbar-fixed-top',
        ],
    ]);
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav navbar-right'],
        'items' => [
            ['label' => 'Home', 'url' => ['/site/index']],
            [
                'label' => 'Alumnos',
                'items' =>[
                    ['label'=>'Consultar Alumno','url'=>Url::toRoute("alumnos/index")],
                    ['label'=>'Crear Alumno','url'=>Url::toRoute("alumnos/create")],
                ],
            ],[
                'label'=>'Profesores',
                'items'=>[
                    ['label'=>'Consultar Profesor','url'=>Url::toRoute("profesor/index")],
                    ['label'=>'Crear Profesor','url'=>Url::toRoute("profesor/create")],
                    ['label'=>'Profesor-Alumno','url'=>Url::toRoute("profesor-alumno/create")],
                    ['label'=>'Ver Alumnos Asignados','url'=>Url::toRoute("profesor-alumno/index")],
                ],
            ],[
                'label'=>'Acudientes',
                'items'=>[
                    ['label'=>'Consultar Acudiente','url'=>Url::toRoute("acudientes/index")],
                    ['label'=>'Crear Acudiente','url'=>Url::toRoute("acudientes/create")],
                    ['label'=>'Alumno-Acudiente','url'=>Url::toRoute("estudiante-acudiente/create")],
                    ['label'=>'Ver Alumno-Acudiente','url'=>Url::toRoute("estudiante-acudiente/index")],                    
                ],
            ],[
                'label'=>'Asignaturas',
                'items'=>[
                    ['label'=>'Consultar Asignatura','url'=>Url::toRoute("asignaturas/index")],
                    ['label'=>'Crear Asignatura','url'=>Url::toRoute("asignaturas/create")],
                    ['label'=>'Asociar Profesor','url'=>Url::toRoute("asignaturas/asignarprofesor")],
                ],
            ],            
            /*['label' => 'Contact', 'url' => ['/site/contact']],
            
            Yii::$app->user->isGuest ? (
                ['label' => 'Login', 'url' => ['/site/login']]
            ) : (
                '<li>'
                . Html::beginForm(['/site/logout'], 'post')
                . Html::submitButton(
                    'Logout (' . Yii::$app->user->identity->username . ')',
                    ['class' => 'btn btn-link logout']
                )
                . Html::endForm()
                . '</li>'
            )*/
        ],
    ]);
    NavBar::end();
    ?>

    <div class="container">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; Imagine Technologies <?= date('Y') ?></p>

        <!--<p class="pull-right"><?= Yii::powered() ?></p>-->
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
