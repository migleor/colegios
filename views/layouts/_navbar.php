<?php
use cebe\gravatar\Gravatar;
use yii\web\Session;
use yii\helpers\Url;
use app\models\Usuarios;
use app\models\Profesor;
use app\models\Acudientes;
use app\models\Alumnos;
/* @var $this \yii\web\View */
$session = Yii::$app->session;
$usid = $session->get('userid');
if($session->isActive && isset($usid)){
    $usu = Usuarios::findOne($usid);
    $nombre = null;
    $data = null;
    switch($usu->id_perfil){
        case 1:
            $data = Profesor::findOne($usu->id_actor);
        break;
        case 2:
            $data = Alumnos::findOne($usu->id_actor);
        break;
        case 3:
            $data = Acudientes::findOne($usu->id_actor);
        break;
    }
    if($data){
        $nombre = $data->primer_nombre." ".$data->segundo_nombre." ".$data->primer_apellido." ".$data->segundo_apellido;
    }
}
?>
<div id="navbar" class="navbar navbar-default">
    <div class="navbar-container" id="navbar-container">
        <button type="button" class="navbar-toggle menu-toggler pull-left" id="menu-toggler" data-target="#sidebar">
            <span class="sr-only"><?= Yii::t('app', 'Toggle sidebar'); ?></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <div class="navbar-header pull-left">
            <a href="<?= Yii::$app->homeUrl ?>" class="navbar-brand">
                <small>
                    <i class="fa fa-leaf"></i>
                    <?= Yii::$app->name ?>
                </small>
            </a>
        </div>
        <div class="navbar-buttons navbar-header pull-right">
            <ul class="nav ace-nav" style="">
            <?php
            if($session->isActive && isset($usid)){
                ?>
                <li class="light-blue dropdown-modal">
                    <a data-toggle="dropdown" href="#" class="dropdown-toggle">
                        <!--<img class="nav-user-photo" src="assets/images/avatars/user.jpg" alt="Jason's Photo">-->
                        <span class="user-info"><small>Bienvenido,</small><?= ucfirst($data->primer_nombre) ?></span>
                        <i class="ace-icon fa fa-caret-down"></i>
                    </a>
                    <ul class="user-menu dropdown-menu-right dropdown-menu dropdown-yellow dropdown-caret dropdown-close">
                        <!--<li><a href="#"><i class="ace-icon fa fa-cog"></i>Settings</a></li>
                        <li><a href="profile.html"><i class="ace-icon fa fa-user"></i>Profile</a></li>
                        <li class="divider"></li>-->
                        <li><a href="<?= Url::toRoute(["site/logout"])?>"><i class="ace-icon fa fa-power-off"></i>Logout</a></li>
                    </ul>
                </li>       
                <?php
            }else{
                ?>
                <li>
                    <a href="<?= Url::toRoute("site/acceso")?>">
                        <span class="glyphicon glyphicon-user"></span><strong>LOGIN</strong>
                    </a>
                </li>
                <?php
            }
            ?>
            </ul>            
        </div>                 
    </div>
</div>