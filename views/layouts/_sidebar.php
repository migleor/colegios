<?php
use yii\helpers\Html;
use yii\web\Session;
use yii\helpers\Url;
use app\models\Usuarios;
use app\models\Profesor;
use app\models\Alumnos;
use app\models\Acudientes;
/* @var $this \yii\web\View */
$session = Yii::$app->session;
$usid = $session->get('userid');
if($session->isActive && isset($usid)){
    $usu = Usuarios::findOne($usid);
    $nombre = null;
    $data = null;
    switch($usu->id_perfil){
        case 1:
            $data = Profesor::findOne($usu->id_actor);
        break;
        case 2:
            $data = Alumnos::findOne($usu->id_actor);
        break;
        case 3:
            $data = Acudientes::findOne($usu->id_actor);
        break;
    }
    if($data){
        $nombre = $data->primer_nombre." ".$data->segundo_nombre." ".$data->primer_apellido." ".$data->segundo_apellido;
    }
}
/* @var $this \yii\web\View */

$controller = Yii::$app->controller;
?>

<div id="sidebar" class="sidebar responsive">
    <!--<div class="sidebar-shortcuts" id="sidebar-shortcuts">
        <div class="sidebar-shortcuts-large" id="sidebar-shortcuts-large">
            <?php echo Html::a('<i class="ace-icon fa fa-plus"></i>', ['/'], ['class' => 'btn btn-success']); ?>
            <?php echo Html::a('<i class="ace-icon fa fa-book"></i>', ['/'], ['class' => 'btn btn-info']); ?>
            <?php echo Html::a('<i class="ace-icon fa fa-users"></i>', ['/user/admin/index'], ['class' => 'btn btn-warning']); ?>
            <?php echo Html::a('<i class="ace-icon fa fa-signal"></i>', ['/'], ['class' => 'btn btn-danger']); ?>
        </div>
        <div class="sidebar-shortcuts-mini" id="sidebar-shortcuts-mini">
            <?php echo Html::a('', ['/'], ['class' => 'btn btn-success']); ?>
            <?php echo Html::a('', ['/'], ['class' => 'btn btn-info']); ?>
            <?php echo Html::a('', ['/user/admin/index'], ['class' => 'btn btn-warning']); ?>
            <?php echo Html::a('', ['/'], ['class' => 'btn btn-danger']); ?>
        </div>
    </div>-->

    <ul class="nav nav-list">
        <!--<li class="">
            <a href="<?= Yii::$app->homeUrl ?>">
                <i class="menu-icon fa fa-tachometer"></i>
                <span class="menu-text"> Dashboard </span>
            </a>

            <b class="arrow"></b>
        </li>-->

        <li class="<?php echo ($controller && $controller->module->id == 'user' && $controller->id == 'admin') || ($controller && $controller->module->id == 'rbac') ? 'open active' : '' ?>">
            <a href="#" class="dropdown-toggle">
                <i class="menu-icon fa fa-users"></i>
                    <span class="menu-text">
                        Comunidad
                    </span>
                <b class="arrow fa fa-angle-down"></b>
            </a>
            <b class="arrow"></b>
            <ul class="submenu">
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['alumnos/create']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Registrar Estudianes
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['profesor/create']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Registrar Profesores
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['acudientes/create']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Registrar Acudientes
                    </a>
                    <b class="arrow"></b>
                </li>                
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['alumnos/index']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listar Estudianes
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['profesor/index']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listar Profesores
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['acudientes/index']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Listar Acudientes
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['estudiante-acudiente/create']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Asociar Acudientes
                    </a>
                    <b class="arrow"></b>
                </li>
                <li class="<?php echo $controller && $controller->module->id == 'user' && $controller->id == 'admin' && $controller->action->id == 'create' ? 'active' : '' ?>">
                    <a href="<?= Url::to(['profesor-alumno/create']) ?>">
                        <i class="menu-icon fa fa-caret-right"></i>
                        Asociar Alumnos
                    </a>
                    <b class="arrow"></b>
                </li>               
            </ul>
        </li>
        <?php
        if($session->isActive && isset($usid)){
        ?>
        <li class="">
            <a href="#" >
                <i class="menu-icon fa fa-envelope"></i>
                <span class="menu-text">Ver Mensajes</span>
                <b class="arrow"></b>
            </a>          
        </li>
        <li class="">
            <a href="<?= Url::toRoute("profesor/mensajeria")?>" >
                <i class="menu-icon fa fa-share"></i>
                <span class="menu-text">Enviar Mensajes</span>
                <b class="arrow"></b>
            </a>          
        </li>        
        <?php
        }
        ?>
    </ul>
    <div class="sidebar-toggle sidebar-collapse" id="sidebar-collapse">
        <i class="ace-icon fa fa-angle-double-left" data-icon1="ace-icon fa fa-angle-double-left" data-icon2="ace-icon fa fa-angle-double-right"></i>
    </div>
    <script>
        try {
            ace.settings.check('sidebar', 'collapsed');
        } catch (e) {
        }
    </script>
</div>

