<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Perfiles;
use yii\helpers\ArrayHelper;
$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
$username = @$_REQUEST['username'];
$perfil   = @$_REQUEST['perfil'];
?>
<div class="site-login">
    <p>Por favor diligencie los siguientes campos para loguearse:</p>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); 
    ?>
    <?= $form->field($model, 'perfil')->dropDownList(ArrayHelper::map(Perfiles::find()->all(), 'id_perfil', 'nombre_perfil'),["prompt"=>"Seleccionar...","value"=>$perfil]) ?>
    <?= $form->field($model, 'username')->textInput(['value'=>$username]) ?>
    <?php
    if(isset($username)){
        $focus = true;
    }else{
        $focus = false;
    }
    ?>
    <?= $form->field($model, 'password')->passwordInput(['autofocus'=>$focus]) ?>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
