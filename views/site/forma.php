<div class="alert alert-info"><h3>ESTE ES MI PRIMER FORMULARIO EN YII2</h3></div>
<h5 class="alert alert-success"><b><?= $mensaje ?></b></h5>
<?php
use yii\helpers\Url;
use yii\helpers\Html;

echo Html::beginForm(
        Url::toRoute("site/request"),//action
        "get",
        ["class"=>"form-inline"]//options
);
?>
<div class="form-group">
    <?= Html::label("Introduce tu nombre","nombre")?>
    <?= Html::textInput("nombre",null,["class"=>"form-control text-uppercase"]) ?>
</div>
<?= Html::submitInput("Enviar Nombre",["class"=>"btn btn-primary"]) ?>
<?= Html::endForm(); ?>

