<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */
$this->title = 'Mi Panel';
$this->params['breadcrumbs'][] = $this->title;
?>
<!--Panel-->
<div class="card w-50">
    <div class="card-body">
        <h3 class="card-title">Bienvenido: <?= strtoupper($nombre) ?></h3>
        <p class="card-text"><b>Tus Opciones:</b></p>
        <a href="#" class="btn btn-primary">Ver Mensajes</a>
        <a href="<?= Url::toRoute("profesor/mensajeria")?>" class="btn btn-primary">Enviar Mensajes</a>
    </div>
</div>
