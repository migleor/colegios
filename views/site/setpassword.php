<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\SetearPassword;
use yii\helpers\ArrayHelper;
$this->title = 'Asignar Password';
$this->params['breadcrumbs'][] = $this->title;
$userid = $_REQUEST['userid'];
?>
<div class="site-login">
    <h1><?= Html::encode($this->title) ?></h1>
    <?php
        if(isset($msg)&& strlen($msg)>0){
            ?>
            <div class="alert alert-danger">Error al setear el password por favor intente nuevamente</div>
            <?php
        }
    ?>
    <div class="alert alert-info"><b>Por favor cree su password:</b></div>
    <?php $form = ActiveForm::begin(); 
    ?>
        <?= $form->field($modelpass, 'userid')->hiddenInput(['value'=>$userid])->label(false) ?>
        <?= $form->field($modelpass, 'password')->passwordInput() ?>
        <?= $form->field($modelpass, 'passrepeat')->passwordInput() ?>
    <div class="form-group">
        <?= Html::submitButton("Enviar",["class"=>"btn btn-primary"]);?>
    </div>    
    <?php $form->end() ?>
</div>