<?php
use yii\helpers\Url;
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\data\Pagination;
use yii\widgets\LinkPager;
?>
<a href="<?= Url::toRoute("site/createalumno")?>" class="btn btn-success">Crear Alumnos</a>

<?php $f = ActiveForm::begin([
    "method" => "get",
    "action"=> Url::toRoute("site/viewalumno"),
    "enableClientValidation"=>true,
]);
?>
<div class="form form-group">
    <?= $f->field($form,"q")->input("search") ?>
</div>
<?= Html::submitButton("Buscar",["class"=>"btn btn-primary"])?>
<?php $f->end() ?>
<h3><?= $search ?></h3>
<h3>Lista de Alumnos</h3>

<table class="table table-bordered">
    <tr>
        <td>Tipo Documento</td>
        <td>Id. Alumno</td>
        <td>Nombre</td>
        <td>Grado</td>
        <td>Codigo</td>
        <td>Telefono</td>
        <td>Celular</td>
        <td></td>
    </tr>
    <?php
    foreach ($model as $val){
        ?>
        <tr>
            <td><?= $val->tipo_documento?></td>
            <td><?= $val->numero_documento?></td>
            <td><?= strtoupper($val->primer_nombre." ".$val->segundo_nombre." ".$val->primer_apellido." ".$val->segundo_apellido)?></td>
            <td><?= $val->grado ?></td>
            <td><?= $val->codigo?></td>
            <td><?= $val->telefono?></td>
            <td><?= $val->celular?></td>
            <td>
                <a hef="<?= Url::toRoute(["site/updatealumno","id_alumno"=>$val->id_alumno])?>"><span class="glyphicon glyphicon-pencil"></span></a>
                <a hef="#" data-toggle="modal" data-target="#id_alumno_<?= $val->id_alumno?>"><span class="glyphicon glyphicon-trash"></span></a>
                <div class="modal fade" tabindex="-1" role="dialog" id="id_alumno_<?= $val->id_alumno?>">
                  <div class="modal-dialog" role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Eliminar Alumnos</h4>
                      </div>
                      <div class="modal-body">
                        <p>Realmente deseas eliinar el alumno?</p>
                      </div>
                      <div class="modal-footer">
                          <?= Html::beginForm(Url::toRoute("site/deletealumno"),"POST"); ?>
                          <input type="hidden" name="id_alumno" value="<?= $val->id_alumno ?>">
                            <button type="button" class="btn btn-default" data-dismiss="modal">Cerrar</button>
                            <button type="submit" class="btn btn-primary">Aceptar</button>
                          <?php Html::endForm(); ?>  
                      </div>
                    </div><!-- /.modal-content -->
                  </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->                
            </td>
        </tr>    
        <?php
    }
    ?>
</table>
<center>
<?= 
    LinkPager::widget([
        "pagination"=>$pages
    ]);    
?>
</center>
