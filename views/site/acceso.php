<?php
/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model app\models\LoginForm */
use yii\helpers\Html;
use yii\bootstrap\ActiveForm;
use app\models\Perfiles;
use yii\helpers\ArrayHelper;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-login">
    <div class="col-lg-offset-1" style="color:#999;">
        <p>Bienvenido al módulo de colegios, por favor seleccione su perfil e ingrese su cédula
        si es la primer vez que ingresa el sistema<br>le pedirá que genere un password, de lo contrario lo llevará
        al formulario de Login.</p>
    </div>
    <?php $form = ActiveForm::begin([
        'id' => 'login-form',
        'layout' => 'horizontal',
        'fieldConfig' => [
            'template' => "{label}\n<div class=\"col-lg-3\">{input}</div>\n<div class=\"col-lg-8\">{error}</div>",
            'labelOptions' => ['class' => 'col-lg-1 control-label'],
        ],
    ]); 
    ?>
    <div class="row">
        <?= $form->field($model, 'perfil')->dropDownList(ArrayHelper::map(Perfiles::find()->all(), 'id_perfil', 'nombre_perfil'),["prompt"=>"Seleccionar..."],['autofocus' => true]) ?>
        <?= $form->field($model, 'username')->textInput() ?>
    </div>
    <div class="form-group">
        <div class="col-lg-offset-1 col-lg-11">
            <?= Html::submitButton('Login', ['class' => 'btn btn-primary', 'name' => 'login-button']) ?>
        </div>
    </div>
    <?php ActiveForm::end(); ?>
</div>
