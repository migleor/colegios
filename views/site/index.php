<?php

/* @var $this yii\web\View */
use yii\helpers\Url;
$this->title = '<span class="glyphicon glyphicon-globe"></span><strong>BIENVENIDOS A WE - COLEGIOS!</strong>';
?>
<div class="page-content">
    <div class="row">
        <div class="col-xs-6">
            <img src="<?php echo Yii::$app->request->baseUrl; ?>/images/WE.jpg" class="img-fluid img-thumbnail">
        </div>
        <div class="col-xs-6">     
            <h3>Bienvenido a We-Colegios</h3><br>
                <p class="text-justify">En esta comunidad puedes establecer comunicación entre los padres, alumnos y profesores en tiempo real y de manera fácil y segura
            </p>
        </div>
    </div>
</div>

