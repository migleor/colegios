<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use app\models\Grados;
use app\models\Ciudades;
use app\models\TipoDocumento;
use app\models\Parentesco;

$this->title = 'Crear Alumnos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 order-md-1">
    <h2 class="mb-3">CREACIÓN DE ALUMNOS</h2>
    <a href="<?= Url::toRoute("site/viewalumno")?>" class="btn btn-success">Ver Alumnos</a>
    <div class="alert alert-success"><?= $msg ?></div>
    <?php $form = ActiveForm::begin([
        "method" => "post",
        'enableClientValidation' => true,
    ]);
    ?>
    <div class="row"><h3><b>Informacion Personal</b></h3></div>
    <div class="row">
         <div class="col-md-3 mb-3">
            <?= $form->field($model, 'tipo_documento')->dropDownList(ArrayHelper::map(TipoDocumento::find()->all(), 'id_tipo', 'tipo'),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-9 mb-3">
            <?= $form->field($model, "numero_documento")->input("text") ?>   
        </div>       
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "primer_nombre")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "segundo_nombre")->input("text") ?>   
        </div>   
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "primer_apellido")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "segundo_apellido")->input("text") ?>   
        </div>
    </div>
    <div class="row"> 
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'grado')->dropDownList(ArrayHelper::map(Grados::find()->all(), 'id_grado', 'grado'),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "telefono")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "celular")->input("text") ?>   
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'ciudad')->dropDownList(ArrayHelper::map(Ciudades::find()->all(), 'codigo', 'nombre_ciudad'),["prompt"=>"Seleccionar..."]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6 mb-3">
            <?= $form->field($model, "direccion")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "codigo")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "jornada")->input("text") ?>   
        </div>
    </div>
    <div class="row"><h3><b>Informacion Del Acudiente</b></h3></div>
    <div class="row">
         <div class="col-md-3 mb-3">
            <?= $form->field($model, 'parentesco')->dropDownList(ArrayHelper::map(Parentesco::find()->all(), 'id_parentesco', 'parentesco'),["prompt"=>"Seleccionar..."]) ?>
        </div>        
         <div class="col-md-3 mb-3">
            <?= $form->field($model, 'tipo_documento_acudiente')->dropDownList(ArrayHelper::map(TipoDocumento::find()->all(), 'id_tipo', 'tipo'),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-6 mb-3">
            <?= $form->field($model, "documento_acudiente")->input("text") ?>   
        </div>       
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "primer_nombre_acudiente")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "segundo_nombre_acudiente")->input("text") ?>   
        </div>   
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "primer_apellido_acudiente")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "segundo_apellido_acudiente")->input("text") ?>   
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "telefono_acudiente")->input("text") ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, "celular_acudiente")->input("text") ?>
        </div>
        <div class="col-md-6 mb-3">
            <?= $form->field($model, "direccion_acudiente")->input("text") ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-3">
            <?= Html::submitButton("Crear Alumno", ["class" => "btn btn-primary"]) ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>