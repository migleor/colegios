<?php
use yii\helpers\Html;
use yii\helpers\ArrayHelper;
use yii\widgets\ActiveForm;
use app\models\Profesor;
use app\models\Asignaturas;

$this->title = 'Materia Profesor';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="col-md-12 order-md-1">
    <h3 class="mb-3">ASIGNAR MATERIA - PROFESOR</h3>
    <div class="alert alert-success"><?= $msg ?></div>
    <?php $form = ActiveForm::begin([
        "method" => "post",
        'enableClientValidation' => true,
    ]);
    ?>
    <div class="row">
         <div class="col-md-3 mb-3">
            <?= $form->field($model, 'id_profesor')->dropDownList(ArrayHelper::map(Profesor::find()->all(), 'id_profesor', 'primer_nombre'),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'id_materia')->dropDownList(ArrayHelper::map(Asignaturas::find()->all(), 'id_asignatura', 'nombre_asignatura'),["prompt"=>"Seleccionar..."]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <?= Html::submitButton("Asignar", ["class" => "btn btn-primary"]) ?>
        </div>
    </div>
    <?php $form->end() ?>
</div>

