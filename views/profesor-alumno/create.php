<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\ProfesorAlumno */

$this->title = 'Asignar Alumnos';
$this->params['breadcrumbs'][] = ['label' => 'Profesor Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesor-alumno-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'alumnos'=>$alumnos,
        'profesor'=>$profesor,
    ]) ?>

</div>
