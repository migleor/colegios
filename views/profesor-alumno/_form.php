<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
/* @var $this yii\web\View */
/* @var $model app\models\ProfesorAlumno */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="profesor-alumno-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-12 mb-3">
            <?= $form->field($model, 'id_profesor')->dropDownList(ArrayHelper::map($profesor, 'id_profesor',
                    function($profesor){
                        return strtoupper($profesor['primer_nombre'].' '.$profesor['segundo_nombre'].' '.$profesor['primer_apellido'].' '.$profesor['segundo_apellido']);                        
                    }
                ),["prompt"=>"Seleccionar..."]) ?>            
        </div>
    </div>
    <div class="row">
        <div class="col-md-12 mb-3">
            <?= $form->field($model, 'id_alumno')->dropDownList(ArrayHelper::map($alumnos, 'id_alumno',
                    function($alumnos){
                        return strtoupper($alumnos['primer_nombre'].' '.$alumnos['segundo_nombre'].' '.$alumnos['primer_apellido'].' '.$alumnos['segundo_apellido']);                        
                    }
                ),["prompt"=>"Seleccionar..."]) ?>             
        </div>
    </div>
    <div class="row">
        <div class="form-group">
            <?= Html::submitButton('Asignar', ['class' => 'btn btn-success']) ?>
        </div>        
    </div>
    <?php ActiveForm::end(); ?>
</div>
