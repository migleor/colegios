<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\ProfesorAlumno */

$this->title = 'Update Profesor Alumno: ' . $model->id_profesor_alumno;
$this->params['breadcrumbs'][] = ['label' => 'Profesor Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_profesor_alumno, 'url' => ['view', 'id' => $model->id_profesor_alumno]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="profesor-alumno-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
