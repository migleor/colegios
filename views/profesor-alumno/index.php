<?php

use yii\helpers\Html;
use yii\grid\GridView;

/* @var $this yii\web\View */
/* @var $searchModel app\models\ProfesorAlumnoSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Profesor Alumnos';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="profesor-alumno-index">

    <h1><?= Html::encode($this->title) ?></h1>
    <?php // echo $this->render('_search', ['model' => $searchModel]); ?>

    <p>
        <?= Html::a('Create Profesor Alumno', ['create'], ['class' => 'btn btn-success']) ?>
    </p>

    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'filterModel' => $searchModel,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id_profesor_alumno',
            'id_profesor',
            'id_alumno',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>
</div>
