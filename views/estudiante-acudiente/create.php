<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\EstudianteAcudiente */

$this->title = 'Relacionar Estudiante Acudiente';
$this->params['breadcrumbs'][] = ['label' => 'Estudiante Acudientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
if(isset($id_acudiente)){
    $idacu = $id_acudiente;
}else{
    $idacu = "";
}
?>
<div class="estudiante-acudiente-create">
    <?= $this->render('_form', [
        'model' => $model,
        'id_acudiente'=>$idacu
    ]) ?>

</div>
