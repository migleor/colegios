<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\EstudianteAcudiente */

$this->title = 'Update Estudiante Acudiente: ' . $model->id_estudiante_acudiente;
$this->params['breadcrumbs'][] = ['label' => 'Estudiante Acudientes', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_estudiante_acudiente, 'url' => ['view', 'id' => $model->id_estudiante_acudiente]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="estudiante-acudiente-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
    ]) ?>

</div>
