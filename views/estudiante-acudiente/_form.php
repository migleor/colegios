<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\Alumnos;
use app\models\Acudientes;
$alumnos = new Alumnos;
$acudientes = new Acudientes;
/* @var $this yii\web\View */
/* @var $model app\models\EstudianteAcudiente */
/* @var $form yii\widgets\ActiveForm */
?>
<div class="estudiante-acudiente-form">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 mb-3"></div>
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'id_estudiante')->dropDownList(ArrayHelper::map($alumnos->find()->all(), 'id_alumno',
                    function($alumnos){
                        return strtoupper($alumnos['primer_nombre'].' '.$alumnos['segundo_nombre'].' '.$alumnos['primer_apellido'].' '.$alumnos['segundo_apellido']);                        
                    }
                ),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-4 mb-3"></div>
    </div>
    <div class="row">
        <div class="col-md-4 mb-3"></div>
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'id_acudiente')->dropDownList(ArrayHelper::map($acudientes->find()->all(), 'id_acudiente',
                    function($acudientes){
                        return strtoupper($acudientes['primer_nombre'].' '.$acudientes['segundo_nombre'].' '.$acudientes['primer_apellido'].' '.$acudientes['segundo_apellido']);                        
                    }
                ),["prompt"=>"Seleccionar..."]) ?>            
        </div>
        <div class="col-md-4 mb-3"></div>
    </div>
    <div class="form-group">
        <div class="col-md-4 mb-3"></div>
        <div class="col-md-4 mb-3"><?= Html::submitButton('Asociar', ['class' => 'btn btn-success']) ?></div>
        <div class="col-md-4 mb-3"></div>        
    </div>
    <?php ActiveForm::end(); ?>
</div>