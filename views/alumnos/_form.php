<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\ArrayHelper;
use app\models\TipoDocumento;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="form-group">
    <?php $form = ActiveForm::begin(); ?>
    <div class="row">
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'tipo_documento')->dropDownList(ArrayHelper::map(TipoDocumento::find()->all(), 'id_tipo', 'tipo'),["prompt"=>"Seleccionar..."]) ?>
        </div>
        <div class="col-md-8 mb-3">
            <?= $form->field($model, 'numero_documento')->textInput() ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'primer_nombre')->textInput(['style'=>'text-transform:uppercase;']) ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'segundo_nombre')->textInput(['style'=>'text-transform:uppercase;']) ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'primer_apellido')->textInput(['style'=>'text-transform:uppercase;']) ?>
        </div>
        <div class="col-md-3 mb-3">
            <?= $form->field($model, 'segundo_apellido')->textInput(['style'=>'text-transform:uppercase;']) ?>
        </div>         
    </div>
    <div class="row">
        <div class="col-md-4 mb-3">             
            <?= $form->field($model, 'telefono')->textInput(["class"=>"form-control input-mask-phone"])->widget(\yii\widgets\MaskedInput::className(),["mask"=>"9999999"]) ?>
        </div>
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'celular')->textInput(["class"=>"form-control input-mask-phone"])->widget(\yii\widgets\MaskedInput::className(),["mask"=>"9999999999"]) ?> 
        </div>
        <div class="col-md-4 mb-3">
            <?= $form->field($model, 'email')->textInput() ?>
        </div>        
    </div>
    <div class="form-group">
        <?= Html::submitButton('Crear Alumno', ['class' => 'btn btn-info']) ?>
    </div>
    <?php ActiveForm::end(); ?>
</div>
