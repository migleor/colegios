<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = 'Actualizar Alumno: ' . strtoupper($model->primer_nombre.' '.$model->segundo_nombre.' '.$model->primer_apellido.' '.$model->segundo_apellido);
$this->params['breadcrumbs'][] = ['label' => 'Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id_alumno, 'url' => ['view', 'id' => $model->id_alumno]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="alumnos-update">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
