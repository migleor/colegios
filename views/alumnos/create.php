<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\Alumnos */

$this->title = 'Crear Alumnos';
$this->params['breadcrumbs'][] = ['label' => 'Alumnos', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="alumnos-create">
    <?= $this->render('_form', ['model' => $model,]) ?>
</div>
