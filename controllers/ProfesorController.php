<?php
namespace app\controllers;
use Yii;
use app\models\Profesor;
use app\models\ProfesorSearch;
use app\models\Alumnos;
use app\models\Acudientes;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\Mensajeria;
use yii\web\Session;
use app\models\Usuarios;
use app\models\EstudianteAcudiente;
use app\models\ProfesorAlumno;

/**
 * ProfesorController implements the CRUD actions for Profesor model.
 */
class ProfesorController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all Profesor models.
     * @return mixed
     */
    public function actionIndex()
    {
        $searchModel = new ProfesorSearch();
        $dataProvider = $searchModel->search(Yii::$app->request->queryParams);

        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Profesor model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Profesor model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Profesor();

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_profesor]);
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Profesor model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);

        if ($model->load(Yii::$app->request->post()) && $model->save()) {
            return $this->redirect(['view', 'id' => $model->id_profesor]);
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Profesor model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    /**
     * Finds the Profesor model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Profesor the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Profesor::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
    public function actionMensajeria(){
        $model = new Mensajeria;
        $usu = null;
        $msg = null;
        if ($model->load(Yii::$app->request->post())){
            if($model->validate()){
                //se trabaja con los id de la tabla usuarios ara facilitar las busquedas de los mensajes, traemos el id de usuario del destinatario
               $usuDestinatario = $model->getUsuarioActor($model->id_destinatario);
               $celuDestinatario = $model->getInfoUsuario($usuDestinatario,'celular');
               $mailDestinatario = $model->getInfoUsuario($usuDestinatario, 'email');
               $idDestinatario = $model->id_destinatario;
               $mensaje = $model->mensaje;
               $remitente = $model->id_remitente;
               $param = [
                   "asunto"=>$model->asunto,
                   "id_remitente"=>$model->id_remitente,
                   "id_destinatario"=>$usuDestinatario,
                   "perfil_remitente"=>$model->perfil_remitente,
                   "mensaje"=>$model->mensaje
               ];
               $model->id_destinatario = $usuDestinatario;
               $model->save();
               //enviamos el mensaje de texto
               Yii::$app->sms->send(array('to'=>$celuDestinatario, 'message'=>'Hola!! We-Colegios te informa que tienes un nuevo mensaje, revisa tu correo o ingresa a lm0.eu/uI8TtzY'));
               if($mailDestinatario!=""){
                   $model->sendMail($param['mensaje'], $mailDestinatario, $param['asunto']);
               }
               switch($param['perfil_remitente']){
                    case 1:
                        //en mensaje lo envia un profesor luego debe llegar al alumno y a sus acudientes                       
                        //insertamos el registro de mensaje en la tabla
                        $acudientes = EstudianteAcudiente::find()
                            ->where(["id_estudiante"=>$idDestinatario])
                            ->all();
                        $idacu = null;
                        //enviamos el correo al estudiante
                        foreach($acudientes as $key){
                            $idacu   = $model->getUsuarioActor($key->id_acudiente);
                            $celacu  = $model->getInfoUsuario($idacu,'celular');
                            $mailacu = $model->getInfoUsuario($idacu, 'email');
                            Yii::$app->db->createCommand()->insert('mensajeria', [
                                'asunto'=>$param['asunto'],
                                'id_remitente'=>$param['id_remitente'],
                                'id_destinatario'=>$idacu,
                                'perfil_remitente'=>$param['perfil_remitente'],
                                'mensaje'=>$param['mensaje']
                            ])->execute();
                            //enviamos el mensaje de texto a cada acudiente
                            if($celacu==""){
                                Yii::$app->sms->send(array('to'=>$celacu, 'message'=>'Hola!! We-Colegios te informa que tienes un nuevo mensaje, revisa tu correo o ingresa a lm0.eu/uI8TtzY'));
                            }
                            if($mailacu!=""){
                                $model->sendMail($param['mensaje'], $mailacu, $param['asunto']);
                            }
                        }
                        $model->id_destinatario = null;
                        $model->asunto = null;
                        $model->mensaje = null;
                        $msg = "<div class='alert alert-info'><b>Mensaje enviado exitosamente</b></div>";
                    break;
                    case 2:
                        //el mensaje lo envia un alumno debemos su enviarlo a su acudiente.
                        $acudientes = EstudianteAcudiente::find()
                            ->where(["id_estudiante"=>$param['id_remitente']])
                            ->all();
                        $idacu = null;
                        foreach($acudientes as $key){
                            $idacu = $model->getUsuarioActor($key->id_acudiente);
                            $celacu = $celuDestinatario = $model->getInfoUsuario($idacu,'celular');
                            Yii::$app->db->createCommand()->insert('mensajeria', [
                                'asunto'=>$param['asunto'],
                                'id_remitente'=>$param['id_remitente'],
                                'id_destinatario'=>$idacu,
                                'perfil_remitente'=>$param['perfil_remitente'],
                                'mensaje'=>$param['mensaje']
                            ])->execute();
                            //enviamos el mensaje de texto a cada acudiente
                            if($celacu==""){
                                Yii::$app->sms->send(array('to'=>$celacu, 'message'=>'Hola!! We-Colegios te informa que tienes un nuevo mensaje, revisa tu correo o ingresa a lm0.eu/uI8TtzY'));
                            }
                        }
                        $model->id_destinatario = null;
                        $model->asunto = null;
                        $model->mensaje = null;
                        $msg = "<div class='alert alert-info'><b>Mensaje enviado exitosamente</b></div>";                        
                    break;
                    case 3:
                        //el mensaje lo envia un acudiente, a su hijo y a los otros acudientes de ese hijo.
                        $celus = array();
                        $hijos = EstudianteAcudiente::find()
                                ->where(["id_acudiente"=>$param["id_remitente"]])
                                ->all();
                        foreach($hijos as $row){
                            $idhijo = $model->getUsuarioActor($row->id_estudiante);
                            $celhijo = $celuDestinatario = $model->getInfoUsuario($idhijo,'celular');
                            Yii::$app->db->createCommand()->insert('mensajeria', [
                                'asunto'=>$param['asunto'],
                                'id_remitente'=>$param['id_remitente'],
                                'id_destinatario'=>$idhijo,
                                'perfil_remitente'=>$param['perfil_remitente'],
                                'mensaje'=>$param['mensaje']
                            ])->execute();
                            //enviamos el mensaje de texto a cada acudiente
                            if($celhijo==""){
                                $celus[] = $celhijo;
                            }
                            //ahora traemos los celulares de los otros acudientes menos del que envió el mensaje*/
                            $acudientes = EstudianteAcudiente::find()
                                ->where(["!=","id_acudiente",$remitente])
                                ->andWhere(["id_estudiante"=>$row->id_estudiante])
                                ->all();
                            foreach($acudientes as $key){
                                $idacu = $model->getUsuarioActor($key->id_acudiente);
                                $celacu = $celuDestinatario = $model->getInfoUsuario($idacu,'celular');
                                Yii::$app->db->createCommand()->insert('mensajeria', [
                                    'asunto'=>$param['asunto'],
                                    'id_remitente'=>$param['id_remitente'],
                                    'id_destinatario'=>$idacu,
                                    'perfil_remitente'=>$param['perfil_remitente'],
                                    'mensaje'=>$param['mensaje']
                                ])->execute();
                                //enviamos el mensaje de texto a cada acudiente
                                if($celacu==""){
                                    $celus[] = $celacu;
                                }                              
                            }
                        }
                        $celulares = array_unique($celus);
                        foreach ($celulares as $cel){
                            Yii::$app->sms->send(array('to'=>$cel, 'message'=>'Hola!! We-Colegios te informa que tienes un nuevo mensaje, revisa tu correo o ingresa a lm0.eu/uI8TtzY'));
                        }
                        $model->id_destinatario = null;
                        $model->asunto = null;
                        $model->mensaje = null;
                        $msg = "<div class='alert alert-info'><b>Mensaje enviado exitosamente</b></div>";                        
                    break;                
                }
                
            }
        }
        $session = Yii::$app->session;
        $usid = $session->get('userid');
        $perfil = $session->get('perfil');
        $table = null;
        if($session->isActive && isset($usid)){
            switch($perfil){
                case 1:
                    $table = new Alumnos;
                break;
                case 2:
                    $table = new Profesor;
                break;
                case 3:
                    $table = new Profesor;
                break;            
            }
            return $this->render("mensajeria",
                    [
                        "model"=>$model,
                        "msg"=>$msg,
                        "usuario"=>$usid,
                        "perfil"=>$perfil,
                        "table"=>$table
                    ]
            );
        }else{
            $this->redirect(["site/index"]);
        }
    }
}
