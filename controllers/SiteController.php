<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\AccesoForm;
use app\models\ContactForm;
use app\models\ValidaForma;
use app\models\ValidaFormaAjax;
use app\models\SetearPassword;
use app\models\Usuarios;
use yii\widgets\ActiveForm;
use app\models\FormAlumnos;
use app\models\Alumnos;
use app\models\Grados;
use app\models\AlumnoSearch;
use yii\helpers\Html;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\web\Session;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function actionPanel(){
        $session = Yii::$app->session;
        $user_id = $session->get('userid');
        $usu = Usuarios::findOne($user_id);
        $nombre = null;
        $data = null;
        switch($usu->id_perfil){
            case 1:
                $data = \app\models\Profesor::findOne($usu->id_actor);
            break;
            case 2:
                $data = Alumnos::findOne($usu->id_actor);
            break;
            case 3:
                $data = \app\models\Acudientes::findOne($usu->id_actor);
            break;
        }
        if($data){
            $nombre = $data->primer_nombre." ".$data->segundo_nombre." ".$data->primer_apellido." ".$data->segundo_apellido;
        }
        return $this->render("panel",["userid"=>$user_id,"perfil"=>$usu->id_perfil,"nombre"=>$nombre]);
    }
    public function actiondeletealumno(){
        if(Yii::$app->request->post()){
            $id_alumno = Html::encode($_POST['id_alumno']);
            if((int) $id_alumno){
                if(Alumnos::deleteAll("id_alumno=:id_alumno",[":id_alumno"=>$id_alumno])){
                    echo "Alumno Eliminado Exitosamente";
                    echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("site/viewalumno")."'>";
                }else{
                    echo "Ha ocurrido un error al eliminar el alumno, redireccionando ...";
                    echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("site/viewalumno")."'>";                     
                }
            }else{
                echo "Ha ocurrido un error al eliminar el alumno, redireccionando ...";
                echo "<meta http-equiv='refresh' content='3; ".Url::toRoute("site/viewalumno")."'>";                
            }
        }else{
            return $this->redirect("site/viewalumno");
        }
        //return $this->render("deleteAlumno");
    }
    public function actionViewalumno(){
        /*$table  = new Alumnos;
        $model  = $table->find()->all();
        $form   = new AlumnoSearch;
        $search = null;
        if($form->load(Yii::$app->request->get())){
            if($form->validate()){
                $search = Html::encode($form->q);
                $query = "Select * from alumnos where tipo_documento ilike '%$search%' OR ";
                $query .= " numero_documento ilike '%$search%' OR ";
                $query .= " primer_nombre ilike '%$search%' OR";
                $query .= " segundo_nombre ilike '%$search%' OR";
                $query .= " primer_apellido ilike '%$search%' OR";
                $query .= " segundo_apellido ilike '%$search%'";
                $model = $table->findBySql($query)->all();
            }else{
                $form->getErrors();
            }
        }*/
        $form = new AlumnoSearch;
        $search = null;
        if($form->load(Yii::$app->request->get())){
            if($form->validate()){
                $search  = Html::encode($form->q);
                $table = Alumnos::find()->where(["ilike","tipo_documento",$search])
                        ->orWhere(["ilike","numero_documento",$search])
                        ->orWhere(["ilike","primer_nombre",$search])
                        ->orWhere(["ilike","segundo_nombre",$search])
                        ->orWhere(["ilike","primer_apellido",$search])
                        ->orWhere(["ilike","segundo_apellido",$search]);
                $count = clone $table;
                $pages = new Pagination([
                    "pageSize"=>1,
                    "totalCount" => $count->count()
                ]);
                $model = $table
                        ->offset($pages->offset)
                        ->limit($pages->limit)
                        ->all();
            }else{
                $form->getErrors();
            }
        }else{
            $table = Alumnos::find();
            $count = clone $table;
            $pages = new Pagination([
                "pageSize"=>1,
                "totalCount" => $count->count()
            ]);
            $model = $table
                    ->offset($pages->offset)
                    ->limit($pages->limit)
                    ->all();
        }
        return $this->render("viewalumno",["model"=>$model,"form"=>$form,"search"=>$search,"pages"=>$pages]);
    }
    public function actionCreatealumno(){
        $model = new FormAlumnos;
        $msg = null;
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                $table = new Alumnos;
                $table->tipo_documento = strtoupper($model->tipo_documento);
                $table->numero_documento = $model->numero_documento;
                $table->primer_nombre = strtoupper($model->primer_nombre);
                $table->segundo_nombre = strtoupper($model->segundo_nombre);
                $table->primer_apellido = strtoupper($model->primer_apellido);
                $table->segundo_apellido = strtoupper($model->segundo_apellido);
                $table->grado = $model->grado;
                $table->telefono = $model->telefono;
                $table->celular = $model->celular;
                $table->direccion = strtoupper($model->direccion);
                $table->id_jornada = $model->jornada;
                $table->id_ciudad = $model->ciudad;
                $table->codigo = $model->codigo;
                $table->tipo_documento_acudiente = $model->tipo_documento_acudiente;
                $table->documento_acudiente = $model->documento_acudiente;
                $table->primer_nombre_acudiente = $model->primer_nombre_acudiente;
                $table->segundo_nombre_acudiente = $model->segundo_nombre_acudiente;
                $table->primer_apellido_acudiente = $model->primer_apellido_acudiente;
                $table->segundo_apellido_acudiente = $model->segundo_apellido_acudiente;
                $table->parentesco_acudiente = $model->parentesco;
                $table->telefono_acudiente = $model->telefono_acudiente;
                $table->celular_acudiente = $model->celular_acudiente;
                $table->direccion_acudiente = $model->direccion_acudiente;
                if($table->insert()){
                    $msg= "Alumno Creado Exitosamente";
                    $model->tipo_documento = null;
                    $model->numero_documento = null;
                    $model->primer_nombre = null;
                    $model->segundo_nombre = null;
                    $model->primer_apellido = null;
                    $model->segundo_apellido = null;
                    $model->grado = null;
                    $model->telefono = null;
                    $model->celular = null;
                    $model->direccion = null;
                    $model->jornada = null;
                    $model->ciudad = null;
                    $model->codigo = null;
                    $model->tipo_documento_acudiente = null;
                    $model->documento_acudiente = null;
                    $model->primer_nombre_acudiente = null;
                    $model->segundo_nombre_acudiente = null;
                    $model->primer_apellido_acudiente = null;
                    $model->segundo_apellido_acudiente = null;
                    $model->parentesco = null;
                    $model->telefono_acudiente = null;
                    $model->celular_acudiente = null;
                    $model->direccion_acudiente = null;
                }else{
                    $msg = "Ha ocurrido un error al insertar el registro";
                }   
            }else{
                $model->getErrors();
            }
        }
        return $this->render("createalumno",['model'=>$model,'msg'=>$msg]);
    }
    public function actionCreateprofesor(){
        return $this->render("createprofesor");
    }
    public function actionSaludo($get="Tutorial Yii"){
        $mensaje = "Hola mundo Yii2";
        $numeros = [0,1,2,3,4];
        return $this->render("saludo",["mensaje"=>$mensaje,"datos"=>$numeros,"get"=>$get]);
    }
    public function actionForma($mensaje=null){
        return $this->render("forma",["mensaje"=>$mensaje]);
    }
    public function actionRequest(){
        $mensaje = "";
        if(isset($_REQUEST["nombre"])){
            $mensaje = "El nombre enviado es ".strtoupper($_REQUEST['nombre']);
        }
        $this->redirect(["site/forma","mensaje"=>$mensaje]);
    }
    public function actionValidaforma(){
        $model = new ValidaForma;
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                //por ejemplo consultar en una base de datos
            }else{
                $model->getErrors();
            }
        }
        return $this->render("validaforma",["model"=>$model]);
    }
    public function actionValidaformaajax(){
        $model = new ValidaFormaAjax;
        $msg   = null;
        if($model->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                //hacer consulta a base de datoa
                $msg = "Datos enviados correctamente";
                $model->nombre = null;
                $model->email = null;
            }else{
                $model->getErrors();
            }
        }
        return $this->render("validaformaajax",["model"=>$model,"msg"=>$msg]);
    } 
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }

    /**
     * Login action.
     *
     * @return Response|string
     */
    public function actionLogin(){
        $model = new LoginForm;
        $session = Yii::$app->session;
        if($session->isActive){
            $session->destroy();
       }
       if($model->load(Yii::$app->request->post())){
           if($model->validate()){
               //acceso correcto creamos una sessión para manejarla en el sistema
               $session->open();
               $session->set('userid', $model->userid);
               $session->set('perfil', $model->perfil);
               Yii::$app->response->redirect(["site/panel"]);
           }else{
               $model->getErrors();
           }
       }
        return $this->render('login', [
            'model' => $model,
            'perfil'=>null,
            'username'=>null
        ]);         
    }
    public function actionAcceso(){
        $session = Yii::$app->session;
        if($session->isActive){
            $session->close();
            $session->destroy();
        }
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }
        $msg = null;
        $model = new AccesoForm();
        if($model->load(Yii::$app->request->post())){
            if($model->validate()){
                $data = $model->firstTime();
                if($data["status"]=="setear"){
                    $userid = $data["userid"];
                    Yii::$app->response->redirect(["site/setpassword","userid"=>$userid]);
                }else if($data["status"]=='login'){
                    Yii::$app->response->redirect(["site/login","perfil"=>$data["perfil"],"username"=>$data["username"]]);
                }
            }
        }
        return $this->render('acceso', [
            'model' => $model,
        ]);        
    }
    public function actionSetpassword(){ 
        $modelpass = new SetearPassword();
        if($modelpass->load(Yii::$app->request->post()) && Yii::$app->request->isAjax){
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($modelpass);
        }
        if($modelpass->load(Yii::$app->request->post())){
            if($modelpass->validate()){
                $id = $modelpass->userid;              
                $setPass = Usuarios::findOne($id);
                $setPass->password = base64_encode($modelpass->password);
                if ($setPass->update()){
                    $model = new LoginForm;
                    Yii::$app->response->redirect(["site/login"]);
                }else{
                    return $this->render('setpassword',[
                        'modelpass'=>$modelpass,
                        'msg'=>''
                    ]);                    
                }
            }else{
                $modelpass->getErrors();
            }
        }                
        return $this->render('setpassword',[
            'modelpass'=>$modelpass,
            'msg'=>''
        ]);
    }
    /**
     * Logout action.
     *
     * @return Response
     */
    public function actionLogout()
    {
        $session = Yii::$app->session;
        $session->close();
        $session->destroy();
        return $this->redirect(["site/acceso"]);
    }

    /**
     * Displays contact page.
     *
     * @return Response|string
     */
    public function actionContact()
    {
        $model = new ContactForm();
        if ($model->load(Yii::$app->request->post()) && $model->contact(Yii::$app->params['adminEmail'])) {
            Yii::$app->session->setFlash('contactFormSubmitted');

            return $this->refresh();
        }
        return $this->render('contact', [
            'model' => $model,
        ]);
    }

    /**
     * Displays about page.
     *
     * @return string
     */
    public function actionAbout()
    {
        return $this->render('about');
    }
}
